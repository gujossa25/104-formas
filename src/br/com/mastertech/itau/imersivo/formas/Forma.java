package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public abstract class Forma {
	private ArrayList<Double> lados;
	private double area;

	public ArrayList<Double> getLados() {
		return lados;
	}

	public void setLados(ArrayList<Double> lados) {
		this.lados = lados;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	} 
	public Forma calcular(Forma forma) {
		return forma;
	}
	
}
