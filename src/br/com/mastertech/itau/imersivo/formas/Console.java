package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.management.RuntimeErrorException;

public class Console {
	public static void boasVindas() {
		
		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");
	}
	public static ArrayList<Double> coletaLados() {
		ArrayList<Double> lados = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
				
		boolean deveAdicionarNovoLado = true;
		while(deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (lados.size() + 1));
			
			double tamanhoLado = Double.parseDouble(scanner.nextLine());
			
			if(tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			}else {
				lados.add(tamanhoLado);
			}
		}
		System.out.println("Lados cadastrados!");
		System.out.println("Agora vamos calcular a área...");
		return lados;
	}
	public static Forma calcularArea(Forma forma) {
		ArrayList<Double> lados = new ArrayList<>();
		lados = forma.getLados();
		try {
			if(lados.size() == 0) {
				throw new RuntimeException("Forma inválida!");
			}else if(lados.size() == 1) {
				Circulo circulo = new Circulo();
				circulo.setLados((ArrayList<Double>) (lados));
				return circulo.calcular(circulo);
			}else if(lados.size() == 2) {
				Quadrado quadrado = new Quadrado();
				quadrado.setLados((ArrayList<Double>) (lados));
				return quadrado.calcular(quadrado);
			}else if(lados.size() == 3) {
				Triangulo triangulo = new Triangulo();
				triangulo.setLados((ArrayList<Double>) (lados));
				return triangulo.calcular(triangulo);
			}else {
				throw new RuntimeException("Ops! Eu não conheço essa forma geometrica ¯\\\\_(⊙_ʖ⊙)_/¯");
			}
		}catch (RuntimeException e) {
			throw e;
		}
	}
	public static void imprimir(Forma forma) {
		System.out.println(forma);
		
	}
	public static void imprimir(String mensagem) {
		System.out.println(mensagem);
	}
}
