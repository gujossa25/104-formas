package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Triangulo extends Forma{
	public Forma calcular(Forma triangulo) {
		double ladoA = triangulo.getLados().get(0);
		double ladoB = triangulo.getLados().get(1);
		double ladoC = triangulo.getLados().get(2);
		if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			double s = (ladoA + ladoB + ladoC) / 2;       
			this.setArea(Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC)));
		}else {
			
			this.setArea(0);
		}
		return this;
	}
	public String toString() {
		String retorno = "Eu identifiquei um triangulo!\n";
		if (this.getArea() > 0) {
			retorno +=  "A área do triangulo é " + this.getArea();
		}else {
			retorno += "Mas o triangulo informado era inválido :\n";
		}
		return retorno;
	}

}
