package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Circulo extends Forma{
	
	public Forma calcular(Forma circulo) {
	
		this.setArea((Math.PI * Math.pow(circulo.getLados().get(0), 2)));
		return this; 
	}
	public String toString() {
		String retorno = "Eu identifiquei um circulo!\n";
		retorno += "A área do círculo é " + this.getArea();
		return retorno;
	}
	
	
}
