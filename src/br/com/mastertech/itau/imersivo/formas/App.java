package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Console.boasVindas();
	
		
		List<Double> lados = new ArrayList<>();
		Forma forma = new Forma();
		
		forma.setLados(Console.coletaLados());
		try {
			forma = Console.calcularArea(forma);
			Console.imprimir(forma);
		}catch (Exception e) {
			Console.imprimir(e.getMessage());;
		}
		
	}

}
