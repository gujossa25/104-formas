package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;

public class Quadrado extends Forma{
	
	public Forma calcular(Forma quadrado) {
		this.setArea(quadrado.getLados().get(0) * quadrado.getLados().get(1));
		return this;
		 
	}
	public String toString() {
		String retorno = "Eu identifiquei um quadrado/retangulo!\n";
		retorno += "A área do quadrado/retangulo é " + this.getArea();
		return retorno;
	}
}
